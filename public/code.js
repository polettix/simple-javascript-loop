function random_string(length) {
  var result           = '';
  var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var charactersLength = characters.length;
  for ( var i = 0; i < length; i++ ) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
   return result;
}

function random_strings(n_strings, l_string) {
  var retval = [];
  for (var i = 0; i < n_strings; i++) {
    retval.push(random_string(l_string));
  }
  return retval;
}

function run_tests(n_rounds, n_strings, l_string) {
  var strings_index = random_strings(n_rounds, l_string);
  var total_time = 0;
  var n = 0;
  for (var k = 0; k < n_rounds; k++) {
    var target = random_string(l_string);
    var start = Date.now();
    for (var i = 0; i < n_strings; i++) {
      if (target.localeCompare(strings_index[i]) == 0) {
        n++;
      }
    }
    var end = Date.now();
    total_time = total_time + (end - start);
  }
  return (total_time / n_rounds).toString();
}

function run_simulation() {
  var result = run_tests(
    document.getElementById('n_rounds').value,
    document.getElementById('n_strings').value,
    document.getElementById('l_string').value
  );
  document.getElementById("result-loop").innerHTML =
    'Average duration of one realization: ' + result + ' ms';
}

// ########################################################################
function get_radio_value (name) {
   var rs = document.getElementsByName(name);
   for (var i = 0, l = rs.length; i < l; i++) {
      if (rs[i].checked) {
         return rs[i].value;
      }
   }
}

function run_download() {
   var req = new XMLHttpRequest();
   var start;
   req.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200) {
         var end = Date.now();
         var result = end - start;
         document.getElementById("result-download").innerHTML =
            'Download duration: ' + result + ' ms';
      }
      else {
         document.getElementById("result-download").innerHTML =
            'Wait for it...';
      }
   };
   var bytes = get_radio_value('bytes');
   var items = get_radio_value('items');
   var filename = 'dw/' + items + '-' + bytes + '.json';
   var random_key = random_string(10);
   var random_val = random_string(10);
   var url = filename + '?k' + random_key + '=' + random_val;
   start = Date.now();
   req.open("GET", url, true);
   req.send();
   return false;
}
